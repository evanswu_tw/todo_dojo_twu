import java.util.ArrayList;
import java.util.Arrays;

public class Mommifier {

    // Consider the capital vowels.
    private static final String[] vowels = {"a", "e", "i", "o", "u", "A", "E", "I", "O", "U"};

    public String mommify(String input) {

        if (vowelsRatioLessThanCertainPercent(input)) {
            return input;
        }
        return mommifyByInputByRegex(input);
    }

    private String mommifyByInputByRegex(String input) {
        String str = String.join("", vowels);
        String pattern = "[" + str + "]+";
        // use regex and replaceAll to replace input.
        return input.replaceAll(pattern, "mommy");

    }

    private Boolean vowelsRatioLessThanCertainPercent(String input) {
        double passRatio = 0.33;
        int wordsLength = input.length();
        double ratio;

        // use stream to count vowels.
        Long vowelsCount = Arrays.asList(input.split("")).stream().filter(x -> IsThereAnyVowels(vowels, x)).count();
        ratio = (double) vowelsCount / wordsLength;
        return ratio <= passRatio;
    }

    // good use of stream
    private Boolean IsThereAnyVowels(String[] vowels, String S) {
        return Arrays.stream(vowels).anyMatch(x -> x.contains(S));
    }

}
