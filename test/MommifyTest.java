import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MommifyTest {

    private Mommifier mommifier;

    @Before
    public void startup() {
        mommifier = new Mommifier();
    }

    @Test
    public void shouldEmptyStringReturnEmptyString() {
        assertEquals("", mommifier.mommify(""));
    }

    @Test
    public void shouldConsonantReturnConsonant() {
        assertEquals("h", mommifier.mommify("h"));
    }

    @Test
    public void shouldSingleVowelReturnMommy() {
        assertEquals("mommy", mommifier.mommify("a"));
    }

    @Test
    public void shouldSingleVowelInThreeInutReplaceWithMommy() {
        assertEquals("hmommys", mommifier.mommify("his"));
    }

    @Test
    public void shouldConsecutiveVowelOnlyReplaceOneVowel(){
        assertEquals("hmommyd", mommifier.mommify("head"));

    }
    @Test
    public void ShouldVowelRatioLowerThan33PercentNotChange() {
        //the vowel ratio should more than 33%;
        assertEquals("qewirotybq", mommifier.mommify("qewirotybq"));
    }

    // might want to add test here with capital letters
    @Test
    public void shouldWorksForCapitalLetters(){
        assertEquals("mommyF", mommifier.mommify("AF"));
    }
}
